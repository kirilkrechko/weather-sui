//
//  Weather_SUIApp.swift
//  Weather SUI
//
//  Created by Kiril Krechko on 10.01.24.
//

import SwiftUI

@main
struct Weather_SUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
